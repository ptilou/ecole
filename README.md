```
hugo new site ptilou_ecole
cd ptilou_ecole
```
Copie et extraction du thème via le navigateur de fichiers

Édition du fichier **config.toml** : 
  * changement de baseurl, title, language
  * ajout de theme = "nom-du-theme"

```
hugo new post/premier_billet.md
```

Édition du fichier **premier_billet.md**

hugo server -D
